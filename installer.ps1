# This installer will:
# 1. Detect the target platform, and download the appropriate distribution
# 2. Copy the distribution to the bin directory as `download-secure-files`
# 3. Make `download-secure-files` executable
# 4. Run `download-secure-files`
# Please note:
# * This script is intended for Windows systems
# * Invoke-WebRequest (or alias curl) is required

$os_name = "windows" # Since this is for Windows PowerShell

$architecture = ""
switch ([System.Environment]::Is64BitProcess) {
    True { $architecture = "amd64" }
    False { $architecture = "386" }
}

$current_path = Get-Location

$bin_filename = "download-secure-files-${os_name}-${architecture}"

$download_url = "https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/download-secure-files/-/releases/permalink/latest/downloads/${bin_filename}.exe"

$bin_dir = $args[0]
if ([string]::IsNullOrEmpty($bin_dir)) {
    $bin_dir = "$HOME\bin"
    if (!(Test-Path $bin_dir)) {
        New-Item -ItemType Directory -Force -Path $bin_dir
    }
}

function DisplayError($message) {
    Write-Host "ERROR: $message" -ForegroundColor Red
    exit 1
}

if ([string]::IsNullOrEmpty($bin_dir)) { DisplayError "No destination specified!" }
if (!(Test-Path $bin_dir)) { DisplayError "Failed to create $bin_dir" }
if (!(Get-Command "curl" -ErrorAction SilentlyContinue)) { DisplayError "Could not find curl" }

$tmp_dir = [System.IO.Path]::GetTempPath()
$download_file = Join-Path $tmp_dir $bin_filename

Write-Host "Downloading download-secure-files from ${download_url}"
Invoke-WebRequest -Uri $download_url -OutFile $download_file

Set-Location $bin_dir
if (!(Test-Path $download_file)) {
    Write-Host "Failed to download file"
    exit 1
}

Write-Host "Installing download-secure-files at ${bin_dir}\download-secure-files.exe"
Move-Item -Path $download_file -Destination "${bin_dir}\download-secure-files.exe" -Force

Set-Location $current_path

# PowerShell does not require making a script executable like Bash does
Write-Host "Invoking download-secure-files.exe"
& "${bin_dir}\download-secure-files.exe"
